﻿/// 系统引用
global using System;
global using System.Collections;
global using System.Collections.Generic;
global using System.IO;
global using System.Linq;
global using System.Text;
global using System.Reflection;
global using System.Text.RegularExpressions;
global using Microsoft.Win32;
global using System.ComponentModel;
global using System.Runtime.InteropServices;
global using DockingTools.NFox;
global using System.Drawing;
global using System.Windows.Forms;
global using System.Windows.Forms.Integration;
global using System.Collections.ObjectModel;
global using System.Xml;
global using Microsoft.Xaml.Behaviors;
global using System.Diagnostics;
global using System.Windows;
global using System.Windows.Input;
global using System.Runtime.CompilerServices;
global using System.Windows.Media;
global using System.Reflection.Emit;
global using System.Windows.Markup;
#if GCAD
global using GrxCAD.Runtime;
global using GrxCAD.EditorInput;
global using GrxCAD.ApplicationServices;
global using GrxCAD.DatabaseServices;
global using GrxCAD.DatabaseServices.Filters;
global using GrxCAD.Geometry;
global using GrxCAD.Colors;
global using GrxCAD.Windows;
global using CadApp = GrxCAD.ApplicationServices.Application;
#elif ZCAD
global using ZwSoft.ZwCAD.Runtime;
global using ZwSoft.ZwCAD.EditorInput;
global using ZwSoft.ZwCAD.ApplicationServices;
global using ZwSoft.ZwCAD.DatabaseServices;
global using ZwSoft.ZwCAD.DatabaseServices.Filters;
global using ZwSoft.ZwCAD.Geometry;
global using ZwSoft.ZwCAD.Colors;
global using ZwSoft.ZwCAD.Windows;
global using CadApp = ZwSoft.ZwCAD.ApplicationServices.Application;
#else
global using Autodesk.AutoCAD.Runtime;
global using Autodesk.AutoCAD.EditorInput;
global using Autodesk.AutoCAD.ApplicationServices;
global using Autodesk.AutoCAD.DatabaseServices;
global using Autodesk.AutoCAD.DatabaseServices.Filters;
global using Autodesk.AutoCAD.Geometry;
global using Autodesk.AutoCAD.Colors;
global using Autodesk.AutoCAD.Windows;
global using CadApp = Autodesk.AutoCAD.ApplicationServices.Application;
#endif
/// ifoxcad.basal 引用