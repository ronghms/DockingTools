﻿namespace DockingTools
{
    public class CmdClass : IExtensionApplication
    {
        public void Initialize()
        {
            WpfStartUp.TbPalette();
        }

        public void Terminate()
        {
            throw new NotImplementedException();
        }
        [CommandMethod("DTS")]
        public void DTS()
        {
            WpfStartUp.TbPalette();
        }
         [CommandMethod("Test")]
        public static void Test()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Document doc = CadApp.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            PromptEntityResult per = ed.GetEntity("\n请拾取需要打断的曲线：");
            if (per.Status == PromptStatus.OK)
            {
                ObjectId id = per.ObjectId;
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    BlockTableRecord btr = tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;
                    Entity ent = tr.GetObject(id, OpenMode.ForWrite) as Entity;
                    //Line
                    //if (ent is Curve)
                    //{
                    //    ent.GetSplitCurves

                    //}
                }
            }
        }
    }
    public static partial class WpfStartUp
    {
        public static PaletteSet PSet { get; set; } = null;
        public static void TbPalette()
        {
            if (PSet == null || PSet.Visible == false)
            {
                PSet = new PaletteSet("ToolBox");
                ElementHost host = new()
                {
                    AutoSize = true,
                    Dock = DockStyle.Fill,
                    Child = new DockingFrm()
                };
                PSet.Add("屏幕菜单", host);
                PSet.Visible = true;
                //tbxps.KeepFocus = true;
                //tbxps.Opacity = 50;
                PSet.Size = new System.Drawing.Size(110, 600);
                PSet.Dock = DockSides.Left;
            }
        }
    }
}