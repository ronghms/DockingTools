﻿namespace DockingTools
{
    /// <summary>
    /// DockingFrm.xaml 的交互逻辑
    /// </summary>
    public partial class DockingFrm : System.Windows.Controls.UserControl
    {
        public DockingFrmViewModel FrmViewModel = new();
        public DockingFrm()
        {
            FrmViewModel.LoadCommandFile($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\Cmd.xml");
            InitializeComponent();
            DataContext = FrmViewModel;
        }
    }
}