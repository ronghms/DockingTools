﻿

namespace DockingTools
{
    public class ExItem : ViewModelBase
    {
        private string btnKey;

        public string BtnKey
        {
            get { return btnKey; }
            set { SetProperty(ref btnKey, value); }
        }

        private string btnStr;

        public string BtnStr
        {
            get { return btnStr; }
            set { SetProperty(ref btnStr, value); }
        }
        /// <summary>
        /// 按钮单击
        /// </summary>
        private RelayCommand<string> clickCommand;
        public RelayCommand<string> ClickCommand
        {
            get
            {
                if (clickCommand == null)
                {
                    clickCommand = new RelayCommand<string>(tag =>
                    {
                        CadApp.DocumentManager.MdiActiveDocument.SendStringToExecute("\u001b\u001b" + BtnKey + "\n", true, false, false);
                    },
                    o => { return true; });
                }
                return clickCommand;
            }
        }
    }

    public class DockingFrmViewModel : ViewModelBase
    {
        private Dictionary<string, ObservableCollection<ExItem>> exLst = new();

        public Dictionary<string, ObservableCollection<ExItem>> ExLst
        {
            get { return exLst; }
            set { SetProperty(ref exLst, value); }
        }

        public void LoadCommandFile(string file)
        {
            if (File.Exists(file))
            {
                try
                {
                    XmlDocument doc = new();
                    doc.Load(file);
                    XmlNode root = doc.SelectSingleNode("ScreenMenu");
                    XmlNodeList personNodes = root.ChildNodes;
                    string exTitle = string.Empty;
                    foreach (XmlNode exNode in personNodes)
                    {
                        if (exNode.Name.ToLower() == "group" && exNode.Attributes.Count >= 1 && exNode.Attributes[0].Name.ToLower() == "title")
                        {
                            exTitle = exNode.Attributes[0].Value;
                            ObservableCollection<ExItem> eis = new();
                            if (exNode.HasChildNodes)
                            {
                                foreach (XmlNode btnode in exNode)
                                {
                                    ExItem ei = new();
                                    if (btnode.InnerText != string.Empty && btnode.Name.ToLower() == "item")
                                    {
                                        ei.BtnStr = btnode.InnerText;
                                        string command = string.Empty;
                                        if (btnode.Attributes[0].Name.ToLower() == "command" && command == string.Empty)
                                        {
                                            ei.BtnKey = btnode.Attributes[0].Value;
                                        }
                                    }
                                    eis.Add(ei);
                                }
                            }
                            ExLst.Add(exTitle, eis);
                        }
                    }
                }
                catch { }
            }
        }
    }
}
