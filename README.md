# Generate_AGZ_CAD_DockingTools

#### 介绍
AGZ：A-AutoCAD；G-GrxCAD；Z-ZWCAD；

CADT停靠菜单面板

![输入图片说明](dts.png)

#### 软件架构
需要在
.NET Framework 4.5 以上

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  本项目含有CAD停靠菜单面板（可更改Cmd.xmal中的内容修改面板展示功能选项）Cmd.xml和Microsoft.Xaml.Behaviors.dll文件都需要和DockingToolsxx.dll放在同一个文件夹下
2.  内涵AutoCAD(2015,2017,2021)、中望CAD(2014,2021)、浩辰CAD(2022)版本的制作
3.  添加项目可以直接复制任意.csproj项目文件，进行重名后对里面内容进行更改(注意：三种CAD都含有的.Net版本需要在所有项目文件中全都要包含所有CAD的Nuget包)
    如：在本项目中AutoCAD、中望CAD、浩辰CAD都有.Net4.8，即如下图所示：
    ![输入图片说明](image0.png)
    ![输入图片说明](image1.png)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
